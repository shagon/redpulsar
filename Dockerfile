FROM caddy/caddy:alpine

WORKDIR /app
EXPOSE 2015
COPY public .

CMD [ "/usr/bin/caddy", "file-server", "--root", "/app"]